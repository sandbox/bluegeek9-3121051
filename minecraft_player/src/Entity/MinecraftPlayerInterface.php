<?php

namespace Drupal\minecraft_player\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Player entities.
 *
 * @ingroup minecraft_player
 */
interface MinecraftPlayerInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Player name.
   *
   * @return string
   *   Name of the Player.
   */
  public function getName();

  /**
   * Sets the Player name.
   *
   * @param string $name
   *   The Player name.
   *
   * @return \Drupal\minecraft_player\Entity\MinecraftPlayerInterface
   *   The called Player entity.
   */
  public function setName($name);

  /**
   * Gets the Player creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Player.
   */
  public function getCreatedTime();

  /**
   * Sets the Player creation timestamp.
   *
   * @param int $timestamp
   *   The Player creation timestamp.
   *
   * @return \Drupal\minecraft_player\Entity\MinecraftPlayerInterface
   *   The called Player entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Player revision author.
   *
   * @return null
   *   The user entity for the revision author.
   */
  public function sync();

}
