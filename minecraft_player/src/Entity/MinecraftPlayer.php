<?php

namespace Drupal\minecraft_player\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Player entity.
 *
 * @ingroup minecraft_player
 *
 * @ContentEntityType(
 *   id = "minecraft_player",
 *   label = @Translation("Player"),
 *   handlers = {
 *     "storage" = "Drupal\minecraft_player\MinecraftPlayerStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\minecraft_player\MinecraftPlayerListBuilder",
 *     "views_data" = "Drupal\minecraft_player\Entity\MinecraftPlayerViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\minecraft_player\Form\MinecraftPlayerForm",
 *       "add" = "Drupal\minecraft_player\Form\MinecraftPlayerForm",
 *       "edit" = "Drupal\minecraft_player\Form\MinecraftPlayerForm",
 *       "delete" = "Drupal\minecraft_player\Form\MinecraftPlayerDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\minecraft_player\MinecraftPlayerHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\minecraft_player\MinecraftPlayerAccessControlHandler",
 *   },
 *   base_table = "minecraft_player",
 *   translatable = FALSE,
 *   admin_permission = "administer player entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/minecraft/player/{minecraft_player}",
 *     "edit-form" = "/minecraft/player/{minecraft_player}/edit",
 *     "delete-form" = "/minecraft/player/{minecraft_player}/delete",
 *     "collection" = "/minecraft/player",
 *   },
 *   field_ui_base_route = "minecraft_player.settings"
 * )
 */
class MinecraftPlayer extends ContentEntityBase implements MinecraftPlayerInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => 1, // \Drupal::currentUser()->id(),
    ];
  }

  // /**
  //  * {@inheritdoc}
  //  */
  // protected function urlRouteParameters($rel) {
  //   $uri_route_parameters = parent::urlRouteParameters($rel);

  //   if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
  //     $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
  //   }
  //   elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
  //     $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
  //   }

  //   return $uri_route_parameters;
  // }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Player entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Player entity.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Syncs player with Mojang servers.
   */
  public function sync() {
dpm('Sync extecuted.');
  }

}
