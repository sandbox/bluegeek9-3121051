<?php

namespace Drupal\minecraft_player;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\minecraft_player\Entity\MinecraftPlayerInterface;

/**
 * Defines the storage handler class for Player entities.
 *
 * This extends the base storage class, adding required special handling for
 * Player entities.
 *
 * @ingroup minecraft_player
 */
class MinecraftPlayerStorage extends SqlContentEntityStorage implements MinecraftPlayerStorageInterface {



}
