<?php

namespace Drupal\minecraft_player;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Player entity.
 *
 * @see \Drupal\minecraft_player\Entity\MinecraftPlayer.
 */
class MinecraftPlayerAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\minecraft_player\Entity\MinecraftPlayerInterface $entity */

    switch ($operation) {

      case 'view':

        return AccessResult::allowedIfHasPermission($account, 'view published player entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit player entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete player entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add player entities');
  }


}
