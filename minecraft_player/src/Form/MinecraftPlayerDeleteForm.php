<?php

namespace Drupal\minecraft_player\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Player entities.
 *
 * @ingroup minecraft_player
 */
class MinecraftPlayerDeleteForm extends ContentEntityDeleteForm {


}
