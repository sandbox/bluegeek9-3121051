<?php

namespace Drupal\minecraft_player\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for deleting a Player revision.
 *
 * @ingroup minecraft_player
 */
class MinecraftPlayerRevisionDeleteForm extends ConfirmFormBase {

  /**
   * The Player revision.
   *
   * @var \Drupal\minecraft_player\Entity\MinecraftPlayerInterface
   */
  protected $revision;

  /**
   * The Player storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $minecraftPlayerStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->minecraftPlayerStorage = $container->get('entity_type.manager')->getStorage('minecraft_player');
    $instance->connection = $container->get('database');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'minecraft_player_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the revision from %revision-date?', [
      '%revision-date' => format_date($this->revision->getRevisionCreationTime()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.minecraft_player.version_history', ['minecraft_player' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $minecraft_player_revision = NULL) {
    $this->revision = $this->MinecraftPlayerStorage->loadRevision($minecraft_player_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->MinecraftPlayerStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('content')->notice('Player: deleted %title revision %revision.', ['%title' => $this->revision->label(), '%revision' => $this->revision->getRevisionId()]);
    $this->messenger()->addMessage(t('Revision from %revision-date of Player %title has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%title' => $this->revision->label()]));
    $form_state->setRedirect(
      'entity.minecraft_player.canonical',
       ['minecraft_player' => $this->revision->id()]
    );
    if ($this->connection->query('SELECT COUNT(DISTINCT vid) FROM {minecraft_player_field_revision} WHERE id = :id', [':id' => $this->revision->id()])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.minecraft_player.version_history',
         ['minecraft_player' => $this->revision->id()]
      );
    }
  }

}
