<?php

namespace Drupal\minecraft_player\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Player lookup forms.
 *
 * @ingroup minecraft_player
 */
class MinecraftPlayerLookupForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // Unique ID of the form.
    return 'minecraft_player_lookup_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Create a $form API array.
    $form['name'] = [
      '#type' => 'textfield',
      // '#target_type' => 'minecraft_player',.
      '#title' => $this->t("Name"),
    ];
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Lookup'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate submitted form data.
    // $form['name']['#element_validate'] = [];
    // dpm($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle submitted form data.
    $values = $form_state->cleanValues()->getValues();
    $name = $values['name'];
    $player_storage = \Drupal::entityTypeManager()->getStorage('minecraft_player');
    $client_api = \Drupal::service('http_client_manager.factory')->get('mojang_api_services');
    $client_sessionserver = \Drupal::service('http_client_manager.factory')->get('mojang_sessionserver_services');

    $query = \Drupal::entityQuery('minecraft_player')
      ->condition('name', $name);
    $ids = $query->execute();
    // dpm($ids);
    if (count($ids) > 0) {
      // From database.
      $players = $player_storage->loadMultiple($ids);
      $player = reset($players);
    }
    else {
      // From Mojang.
      $uuidResponse = $client_api->GetUuid(['username' => $name]);
      if (empty($uuidResponse['id'])) {

        $uuidResponse = $client_api->GetUuid(['username' => $name, 'at' => 0]);
        if (empty($uuidResponse['id'])) {
          drupal_set_message(t('The player "@name" does not exist according to Mojang.', ['@name' => $name]), 'warning');
          return;
        }
        // dpm($uuidResponse);
      }
      $name = $uuidResponse['name'];
      $uuid_no_hypthens = $uuidResponse['id'];

      $uuid_array = [];
      $uuid_array[] = substr($uuidResponse['id'], 0, 8);
      $uuid_array[] = substr($uuidResponse['id'], 8, 4);
      $uuid_array[] = substr($uuidResponse['id'], 12, 4);
      $uuid_array[] = substr($uuidResponse['id'], 16, 4);
      $uuid_array[] = substr($uuidResponse['id'], 20, 12);

      $uuid_hypthens = implode('-', $uuid_array);
      $query = \Drupal::entityQuery('minecraft_player')
        ->condition('uuid', $uuid_hypthens);
      $ids = $query->execute();

      if (count($ids) > 0) {
        // From database.
        $players = $player_storage->loadMultiple($ids);
        $player = reset($players);
        $player->set('name', $name);
        dpm('Enity Exists with a different name.');
      }
      else {
        $player = $player_storage->create(['uuid' => $uuid_hypthens, 'name' => $name]);
      }

      $name_history_response = $client_api->GetNameHistory(['uuid' => $uuid_no_hypthens]);
      // dpm($name_history_response);
      $formerly_known_as = [];
      if (count($name_history_response) > 1) {
        foreach ($name_history_response as $name_history) {
          $timestamp = isset($name_history['changedToAt']) ? $name_history['changedToAt'] / 1000 : 0;
          $formerly_known_as[] = [
            'value' => $name_history['name'],
            'timestamp' => $timestamp,
          ];
        }
      }
      usort(
          $formerly_known_as,
          create_function(
              // The list of arguments.
              '$a, $b',
              // The function body (everything you normally put between { and }.
              'global $field; return $b["timestamp"] - $a["timestamp"];'
          )
      );
      $player->field_formerly_known_as = $formerly_known_as;

      $profile_response = $client_sessionserver->GetProfile(['uuid' => $uuid_no_hypthens]);
      if (count($profile_response['properties']) > 0) {
        foreach ($profile_response['properties'] as $property) {
          if ($property['name'] == 'textures') {
            $json_value = base64_decode($property['value']);
            // dpm($json_value);
            $textures_value = json_decode($json_value, TRUE);
            // dpm($textures_value);
            $textures = $textures_value['textures'];

            if (isset($textures['SKIN'])) {
              $skin_url = $textures['SKIN']['url'];
              $slim = isset($textures['SKIN']['metadata']['model']) && $textures['SKIN']['metadata']['model'] == 'slim';
              $cape_url = (isset($textures['CAPE']['url']) ? $textures['CAPE']['url'] : '');
              // dpm($textures);
              $texture_field = [
                'skin' => $skin_url,
                'slim' => $slim,
                'cape' => $cape_url,
              ];

              $player->field_skin = $texture_field;
            }

          }
        }
      }
      // dpm($profile_response);
      $player->save();
      $player->sync();
      // dpm($player);
      $form_state->setRedirect('entity.minecraft_player.canonical', ['minecraft_player' => $player->id()]);
    }

  }

}
