<?php

namespace Drupal\minecraft_player\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnablerBase;

/**
 * Allows MyEntity content to be added to groups.
 *
 * @GroupContentEnabler(
 *   id = "minecraft_player_membership",
 *   label = @Translation("Player Membership"),
 *   description = @Translation("Adds plaers to groups as members."),
 *   entity_type_id = "minecraft_player",
 *   pretty_path_key = "player",
 *   reference_label = @Translation("Player"),
 *   reference_description = @Translation("The player you want to make a member"),
 *   enforced = TRUE
 * )
 */
class GroupMembership extends GroupContentEnablerBase {

}
