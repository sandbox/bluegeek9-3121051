<?php

namespace Drupal\minecraft_player\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\minecraft_player\Entity\MinecraftPlayerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MinecraftPlayerController.
 *
 *  Returns responses for Player routes.
 */
class MinecraftPlayerController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->dateFormatter = $container->get('date.formatter');
    $instance->renderer = $container->get('renderer');

    return $instance;
  }

  /**
   * Displays a Player revision.
   *
   * @param int $minecraft_player_revision
   *   The Player revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($minecraft_player_revision) {
    $minecraft_player = $this->entityTypeManager()->getStorage('minecraft_player')
      ->loadRevision($minecraft_player_revision);
    $view_builder = $this->entityTypeManager()->getViewBuilder('minecraft_player');

    return $view_builder->view($minecraft_player);
  }

  /**
   * Page title callback for a Player revision.
   *
   * @param int $minecraft_player_revision
   *   The Player revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($minecraft_player_revision) {
    $minecraft_player = $this->entityTypeManager()->getStorage('minecraft_player')
      ->loadRevision($minecraft_player_revision);
    return $this->t('Revision of %title from %date', [
      '%title' => $minecraft_player->label(),
      '%date' => $this->dateFormatter->format($minecraft_player->getRevisionCreationTime()),
    ]);
  }

  /**
   * The _title_callback for the page that renders a single node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $minecraft_player
   *   The current minecraft_player.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $minecraft_player) {
    return $minecraft_player->label();
  }

  /**
   * Generates an overview table of older revisions of a Player.
   *
   * @param \Drupal\minecraft_player\Entity\MinecraftPlayerInterface $minecraft_player
   *   A Player object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(MinecraftPlayerInterface $minecraft_player) {
    $account = $this->currentUser();
    $minecraft_player_storage = $this->entityTypeManager()->getStorage('minecraft_player');

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $minecraft_player->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];
    $revert_permission = (($account->hasPermission("revert all player revisions") || $account->hasPermission('administer player entities')));
    $delete_permission = (($account->hasPermission("delete all player revisions") || $account->hasPermission('administer player entities')));

    $rows = [];

    $vids = $minecraft_player_storage->revisionIds($minecraft_player);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\minecraft_player\MinecraftPlayerInterface $revision */
      $revision = $minecraft_player_storage->loadRevision($vid);
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $minecraft_player->getRevisionId()) {
          $link = $this->l($date, new Url('entity.minecraft_player.revision', [
            'minecraft_player' => $minecraft_player->id(),
            'minecraft_player_revision' => $vid,
          ]));
        }
        else {
          $link = $minecraft_player->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => $this->renderer->renderPlain($username),
              'message' => [
                '#markup' => $revision->getRevisionLogMessage(),
                '#allowed_tags' => Xss::getHtmlTagList(),
              ],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => Url::fromRoute('entity.minecraft_player.revision_revert', [
                'minecraft_player' => $minecraft_player->id(),
                'minecraft_player_revision' => $vid,
              ]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.minecraft_player.revision_delete', [
                'minecraft_player' => $minecraft_player->id(),
                'minecraft_player_revision' => $vid,
              ]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
    }

    $build['minecraft_player_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
