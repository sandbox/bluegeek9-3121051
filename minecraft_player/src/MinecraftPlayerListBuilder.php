<?php

namespace Drupal\minecraft_player;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Player entities.
 *
 * @ingroup minecraft_player
 */
class MinecraftPlayerListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['skin'] = [
      'data' => $this->t('Face'),
      'align' => 'center',
    ];
    $header['name'] = [
      'data' => $this->t('Name'),
      'align' => 'left',
    ];
    $header['uuid'] = [
      'data' => $this->t('Uuid'),
      'align' => 'left',
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\minecraft_player\Entity\MinecraftPlayer $entity */
    // $row['id'] = $entity->id();
    $row['skin'] = [
      'data' => $entity->field_skin->view('full'),
      'align' => 'right',
    ];
    $row['name'] = [
      'data' => Link::createFromRoute(
        $entity->label(),
        'entity.minecraft_player.canonical',
        ['minecraft_player' => $entity->id()]
      ),
      'align' => 'left',
    ];
    $row['uuid'] = [
      'data' => $entity->uuid(),
      'align' => 'left',
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['#attached']['library'][] = 'minecraft_skin_field/skin-viewer';

    return $build;
  }

}
