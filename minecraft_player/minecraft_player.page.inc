<?php

/**
 * @file
 * Contains minecraft_player.page.inc.
 *
 * Page callback for Player entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Player templates.
 *
 * Default template: minecraft_player.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_minecraft_player(array &$variables) {
  // Fetch MinecraftPlayer Entity Object.
  $minecraft_player = $variables['elements']['#minecraft_player'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
