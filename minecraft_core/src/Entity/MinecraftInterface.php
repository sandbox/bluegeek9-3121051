<?php

namespace Drupal\minecraft_core\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Minecraft entities.
 *
 * @ingroup minecraft_core
 */
interface MinecraftInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Minecraft name.
   *
   * @return string
   *   Name of the Minecraft.
   */
  public function getName();

  /**
   * Sets the Minecraft name.
   *
   * @param string $name
   *   The Minecraft name.
   *
   * @return \Drupal\minecraft_core\Entity\MinecraftInterface
   *   The called Minecraft entity.
   */
  public function setName($name);

  /**
   * Gets the Minecraft creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Minecraft.
   */
  public function getCreatedTime();

  /**
   * Sets the Minecraft creation timestamp.
   *
   * @param int $timestamp
   *   The Minecraft creation timestamp.
   *
   * @return \Drupal\minecraft_core\Entity\MinecraftInterface
   *   The called Minecraft entity.
   */
  public function setCreatedTime($timestamp);

}
