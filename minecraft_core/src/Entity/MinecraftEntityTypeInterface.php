<?php

namespace Drupal\minecraft_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Minecraft entity type entities.
 */
interface MinecraftEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
