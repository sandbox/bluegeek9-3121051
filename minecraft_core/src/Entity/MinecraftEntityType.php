<?php

namespace Drupal\minecraft_core\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Minecraft entity type entity.
 *
 * @ConfigEntityType(
 *   id = "minecraft_entity_type",
 *   label = @Translation("Minecraft entity type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\minecraft_core\MinecraftEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\minecraft_core\Form\MinecraftEntityTypeForm",
 *       "edit" = "Drupal\minecraft_core\Form\MinecraftEntityTypeForm",
 *       "delete" = "Drupal\minecraft_core\Form\MinecraftEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\minecraft_core\MinecraftEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "minecraft_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "minecraft_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/minecraft/minecraft_entity_type/{minecraft_entity_type}",
 *     "add-form" = "/admin/structure/minecraft/minecraft_entity_type/add",
 *     "edit-form" = "/admin/structure/minecraft/minecraft_entity_type/{minecraft_entity_type}/edit",
 *     "delete-form" = "/admin/structure/minecraft/minecraft_entity_type/{minecraft_entity_type}/delete",
 *     "collection" = "/admin/structure/minecraft/minecraft_entity_type"
 *   }
 * )
 */
class MinecraftEntityType extends ConfigEntityBundleBase implements MinecraftEntityTypeInterface {

  /**
   * The Minecraft entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Minecraft entity type label.
   *
   * @var string
   */
  protected $label;

}
