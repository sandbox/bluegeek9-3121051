<?php

namespace Drupal\minecraft_core\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Minecraft entity entities.
 *
 * @ingroup minecraft_core
 */
interface MinecraftEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Minecraft entity name.
   *
   * @return string
   *   Name of the Minecraft entity.
   */
  public function getName();

  /**
   * Sets the Minecraft entity name.
   *
   * @param string $name
   *   The Minecraft entity name.
   *
   * @return \Drupal\minecraft_core\Entity\MinecraftEntityInterface
   *   The called Minecraft entity entity.
   */
  public function setName($name);

  /**
   * Gets the Minecraft entity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Minecraft entity.
   */
  public function getCreatedTime();

  /**
   * Sets the Minecraft entity creation timestamp.
   *
   * @param int $timestamp
   *   The Minecraft entity creation timestamp.
   *
   * @return \Drupal\minecraft_core\Entity\MinecraftEntityInterface
   *   The called Minecraft entity entity.
   */
  public function setCreatedTime($timestamp);

}
