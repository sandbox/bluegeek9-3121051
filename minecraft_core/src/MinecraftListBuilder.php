<?php

namespace Drupal\minecraft_core;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Minecraft entities.
 *
 * @ingroup minecraft_core
 */
class MinecraftListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Minecraft ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\minecraft_core\Entity\Minecraft $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.minecraft.edit_form',
      ['minecraft' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
